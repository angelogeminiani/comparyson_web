package com.comparyson.server.i18n;


import org.lyj.commons.i18n.DictionaryController;

/**
 * Global static Dictionary Helper.
 */
public class Dictionary
        extends DictionaryController {


    // ------------------------------------------------------------------------
    //                     c o n s t r u c t o r
    // ------------------------------------------------------------------------

    private Dictionary(){

        //-- register all dictionaries --//

        //super.register(Badges.class);

    }


    // ------------------------------------------------------------------------
    //                     p r i v a t e
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    //                     S T A T I C
    // ------------------------------------------------------------------------

    private static DictionaryController __instance;

    public static DictionaryController instance(){
        if(null==__instance){
            __instance = new Dictionary();
        }
        return __instance;
    }



}
