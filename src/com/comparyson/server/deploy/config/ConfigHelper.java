package com.comparyson.server.deploy.config;

import org.lyj.Lyj;
import org.lyj.commons.io.jsonrepository.JsonRepository;
import org.lyj.commons.util.PathUtils;
import org.lyj.commons.util.StringUtils;

/**
 * Direct access to configuration structure
 */
public class ConfigHelper {

    // ------------------------------------------------------------------------
    //                      c o n s t
    // ------------------------------------------------------------------------

    private static final int THUMB_WIDTH = 300;
    private static final int THUMB_HEIGHT = 300;

    private static final String ENABLE_TASK = "lyj.enable_task";

    private static final String API_GOOGLE_MAP = "api.google.map";

    private static final String API_PORT = "server_api.http.port";
    private static final String API_ENABLED = "server_api.http.enabled";
    private static final String WEB_ENABLED = "server_web.http.enabled";
    private static final String WEB_DOMAIN = "server_web.http.domain";
    private static final String WEB_PORT = "server_web.http.port";
    private static final String WEB_404 = "server_web.http.404";
    private static final String WEB_UPLOAD_PATH = "server_web.static.upload.path";
    private static final String WEB_UPLOAD_PATH_TEST = "server_web.static.upload.path_test";
    private static final String WEB_UPLOAD_THUMB_WIDTH = "server_web.static.upload.thumb.width";
    private static final String WEB_UPLOAD_THUMB_HEIGHT = "server_web.static.upload.thumb.height";

    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------

    private final JsonRepository _configuration;

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    private ConfigHelper() {
        _configuration = Lyj.getConfiguration();
    }

    // ------------------------------------------------------------------------
    //                      p u b l i c   ( a p i )
    // ------------------------------------------------------------------------

    public String apiGoogleMap() {
        return _configuration.getString(API_GOOGLE_MAP);
    }

    // ------------------------------------------------------------------------
    //                      p u b l i c   ( w e b )
    // ------------------------------------------------------------------------

    public boolean enableTask() {
        return _configuration.getBoolean(ENABLE_TASK, true);
    }

    public boolean apiEnabled() {
        return _configuration.getBoolean(API_ENABLED);
    }

    public int apiPort() {
        return _configuration.getInt(API_PORT);
    }

    public boolean webEnabled() {
        return _configuration.getBoolean(WEB_ENABLED);
    }

    public String webDomain() {
        final String domain = _configuration.getString(WEB_DOMAIN);
        return StringUtils.hasText(domain)
                ? PathUtils.hasProtocol(domain) ? domain : "http://" + domain
                : "http://localhost";
    }

    public int webPort() {
        return _configuration.getInt(WEB_PORT);
    }

    public String webDomainAndPort() {
        final String domain = this.webDomain();
        final int port = this.webPort();
        if (port > 0) {
            return domain + ":" + port;
        }
        return domain;
    }

    public String apiDomainAndPort() {
        final String domain = this.webDomain();
        final int port = this.apiPort();
        if (port > 0) {
            return domain + ":" + port;
        }
        return domain;
    }

    public String web404() {
        return _configuration.getString(WEB_404);
    }

    public String webUploadPath() {
        return _configuration.getString(Lyj.isTestUnitMode()
                ? WEB_UPLOAD_PATH_TEST
                : WEB_UPLOAD_PATH);
    }

    public int webUploadThumbWidth() {
        return _configuration.getInt(WEB_UPLOAD_THUMB_WIDTH, THUMB_WIDTH);
    }

    public int webUploadThumbHeight() {
        return _configuration.getInt(WEB_UPLOAD_THUMB_HEIGHT, THUMB_HEIGHT);
    }

    // ------------------------------------------------------------------------
    //                     S T A T I C
    // ------------------------------------------------------------------------

    private static ConfigHelper __instance;

    public static ConfigHelper instance() {
        if (null == __instance) {
            __instance = new ConfigHelper();
        }
        return __instance;
    }

}
