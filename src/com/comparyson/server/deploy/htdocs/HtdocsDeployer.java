/*
 * LY (ly framework)
 * This program is a generic framework.
 * Support: Please, contact the Author on http://www.smartfeeling.org.
 * Copyright (C) 2014  Gian Angelo Geminiani
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.comparyson.server.deploy.htdocs;


import com.comparyson.server.deploy.config.ConfigHelper;
import org.lyj.Lyj;
import org.lyj.commons.io.repository.deploy.FileDeployer;
import org.lyj.commons.util.PathUtils;


public class HtdocsDeployer
        extends FileDeployer {


    // ------------------------------------------------------------------------
    //                      c o n s
    // ------------------------------------------------------------------------

    public static final boolean INTERNAL_WEB_SERVER = true;
    private static final String PATH = "htdocs";

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    public HtdocsDeployer(final boolean silent) {
        super("",
                path(),
                silent,
                false,
                false,
                false);
        super.setOverwrite(true); // overwrite default desktopgap index page
        super.settings().excludeFileOrExts().add("/src/*"); // TODO: implement directory exclusion

    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------


    @Override
    public void deploy() {
        super.deploy();
    }

    @Override
    public byte[] compile(byte[] data, final String filename) {
        return data;
    }

    @Override
    public byte[] compress(byte[] data, final String filename) {
        return null;
    }

    // ------------------------------------------------------------------------
    //                      S T A T I C
    // ------------------------------------------------------------------------

    public static String path() {
        if (INTERNAL_WEB_SERVER) {
            return Lyj.getAbsolutePath(PATH);
        } else {
            return PathUtils.concat(ConfigHelper.instance().webUploadPath(), PATH);
        }
    }

}
