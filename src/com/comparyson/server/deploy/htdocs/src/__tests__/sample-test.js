jest.autoMockOff();

import {EventEmitter} from "events";
import _ from "lodash";

class MockEmitter extends EventEmitter {

    privateMethod(){
        console.log('!!!cannot invoke this method!!!');
    }
}

describe('event', ()=> {

    it('catch event', ()=> {
        let emitter = new MockEmitter();
        emitter.on('test', ()=> {
            console.log('Event handled');
        });
        console.log('\nEMITTING EVENT');
        emitter.emit('test');
        emitter.privateMethod();
    });

});

describe('sum', ()=> {

    it('adds 1 + 2 to equal 3', ()=> {
        const sum = (a, b)=> {
            return a + b
        };
        expect(sum(1, 2)).toBe(3);
    });

});

describe('underscore', ()=> {

    it('test underscore', ()=> {
        expect(_.isFunction(()=>{})).toBe(true);
    });

});