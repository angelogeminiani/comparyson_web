// ------------------------------------------------------------------------
//                      i m p o r t
// ------------------------------------------------------------------------

import React, {Component} from "react";
import {render} from "react-dom";

// ------------------------------------------------------------------------
//                      c o n s t
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
//                      c l a s s 
// ------------------------------------------------------------------------

class Explore extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            ready: false,
            loading: false,
            error_code: '',
        };

        //console.log('[Explore]#constructor');
    }

    componentWillMount() {
        //console.log('[Explore]#componentWillMount');
    }

    componentDidMount() {
        //console.log('[Explore]#componentDidMount');
    }

    componentWillUnmount() {
        //console.log('[Explore]#componentWillUnmount');
    }


    render() {
        //console.log('[Explore]#render');
        
        const styles = {
            view:{
                height:window.innerHeight,
                backgroundColor:'rgba(179, 220, 74, 0.85)'
            }
        };
        
        let ui = (
            <div style={styles.view}>
                EXPLORE<br/>
                EXPLORE<br/>
                EXPLORE<br/>
                EXPLORE<br/>
                EXPLORE<br/>
            </div>
        );


        return ui;
    }
}

Explore.propTypes = {
    data: React.PropTypes.object
};
Explore.defaultProps = {data: {}};

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

export default Explore;
