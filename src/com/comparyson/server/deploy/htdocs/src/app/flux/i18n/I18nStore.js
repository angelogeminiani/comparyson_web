import {createStore} from "../../../lib/StoreUtils";
import I18nUtils from "../../../lib/I18nUtils";
//import {I18N_CHANGE_LANG} from "./i18n";
import Dispatcher from "../../ApplicationDisparcher";


const I18N_CHANGE_LANG = 'I18N_CHANGE_LANG';

let _lang = I18nUtils.getLanguage(navigator.language) || 'it';
let _translations = null;

const I18nStore = createStore({

    getLang() {
        return _lang;
    },

    getTranslations(){
        return _translations;
    },
    
    //-- action invoker --//
    setLang(lang){
        const action = {
            type: I18N_CHANGE_LANG,
            lang
        };
        //console.log('Dispatcher.dispatch(action): ', action);
        Dispatcher.dispatch(action); 
    }

});


I18nStore.dispatchToken = Dispatcher.register(action => {
    
    console.log('[I18nStore] Received ACTION: ', action);
    
    if (action.type == I18N_CHANGE_LANG) {
        _lang = I18nUtils.getLanguage(action.lang);
        I18nUtils.loadTranslations(_lang, (translations)=>{
            _translations = translations;
            
            //console.log('Changed Translations: ', translations);
            
            I18nStore.emitChange();
        });
    }
});

export default I18nStore;
