// ------------------------------------------------------------------------
//                      i m p o r t s
// ------------------------------------------------------------------------

import {createStore} from "../../../lib/StoreUtils";
import Dispatcher from "../../ApplicationDisparcher";

// ------------------------------------------------------------------------
//                      c o n s t
// ------------------------------------------------------------------------

const USER_CHANGE = 'USER_CHANGE';

// ------------------------------------------------------------------------
//                      f i e l d s
// ------------------------------------------------------------------------

let _user = null;

// ------------------------------------------------------------------------
//                      s i n g l e t o n
// ------------------------------------------------------------------------

const UserStore = createStore({

    getUser() {
        return _user;
    },

    //-- action invoker --//
    setUser(user){
        const action = {
            type: USER_CHANGE,
            user
        };
        //console.log('Dispatcher.dispatch(action): ', action);
        Dispatcher.dispatch(action);
    }

});


UserStore.dispatchToken = Dispatcher.register(action => {

    console.log('[UserStore] Received ACTION: ', action);

    if (action.type == USER_CHANGE) {
        _user = action.user;
        UserStore.emitChange();
    }
});

export default UserStore;
