/**
 *  test: http://localhost:4000/api/card/read/4267-2541-3335-8864/brand_id redirect to:
 *      http://localhost:8080/#/app_user/4267-2541-3335-8864?_k=ao0qe5
 */
// ------------------------------------------------------------------------
//                      i m p o r t
// ------------------------------------------------------------------------

import React, {Component} from "react";
import {render} from "react-dom";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import I18nStore from "../../flux/i18n/I18nStore";
import UserStore from "../../flux/user/UserStore";
import ServiceCard from "../../services/ServiceCard";
import Paper from "material-ui/Paper";
import Navigator from "./Navigator";
import Home from "./views/home/Home";
import Explore from "./views/explore/Explore";

// ------------------------------------------------------------------------
//                      c o n s t
// ------------------------------------------------------------------------

const styles = {
    paper: {
        padding: 20,
        textAlign: 'left',
        backgroundColor: "#ffffff",
        marginBottom: 50,
        paddingBottom: 60
    },
    page :{
        
    }
};


// ------------------------------------------------------------------------
//                      c l a s s 
// ------------------------------------------------------------------------

class AppUser extends Component {

    constructor(props, context) {
        super(props, context);

        this.card_id = this.props.params.cardId;

        this.state = {
            lang: I18nStore.getLang(),
            translations: I18nStore.getTranslations(),
            user: UserStore.getUser(), // null
            index: 0,
            error_code: ''
        };
    }

    componentWillMount() {
        // i18n listener
        I18nStore.addChangeListener(this.onChangeLang);

        // user listener
        UserStore.addChangeListener(this.onChangeUser);
    }

    componentDidMount() {
        if (!this.card_id) {
            this.setState({error_code: 'err_card_empty_id'});
        } else {
            ServiceCard.getOwner({card_id: this.card_id}, (err, data)=> {
                if (!!err) {
                    console.log('[AppUser]componentDidMount#ServiceCard.getOwner ERROR', err);
                    this.setState({error_code: err});
                } else {
                    console.log('[AppUser]componentDidMount#ServiceCard.getOwner SUCCESS', data);
                    this.setState({user: data});
                }
            });
        }
    }

    componentWillUnmount() {
        I18nStore.removeChangeListener(this.onChangeLang);
        UserStore.removeChangeListener(this.onChangeUser);
    }

    onChangeLang() {
        this.setState({
            lang: I18nStore.getLang(),
            translations: I18nStore.getTranslations()
        });
    }

    onChangeUser() {
        this.setState({
            user: UserStore.getUser()
        });
    }

    onNavigatorAction(event, action) {
        if (action === 'explore') {
            //console.log('onNavigatorAction', action);
            this.setState({index: 1});
        }
    }

    render() {
        const self = this;

        const labels = {
            menu_home: this.state.translations.app_user.menu_home,
            menu_explore: this.state.translations.app_user.menu_explore,

            error: !!this.state.error_code ? this.state.translations.error[this.state.error_code] : ''
        };

        let ui = (
            <div>
                <img className="bg" src="./assets/img/bg/shopping_bags.jpg"/>
            </div>
        );

        if (!!this.state.error_code) {
            const message = labels.error || this.state.error_code;
            ui = (
                <div>

                    <img className="bg" src="./assets/img/bg/shopping_bags.jpg"/> 

                    <ReactCSSTransitionGroup transitionName="fade" transitionEnterTimeout={500}
                                             transitionLeaveTimeout={300}>
                        <div style={styles.page}>
                            <Paper className="vcenter" style={styles.paper} zDepth={5}>
                                <h2>{message}</h2>
                            </Paper>
                        </div>
                    </ReactCSSTransitionGroup>
                </div>
            );
        } else if (!!this.state.user) {
            const context_data = {user: this.state.user, cardId: this.card_id};
            const on_action = self.onNavigatorAction.bind(self);
            const index = this.state.index;
            //console.log('INDEX', index);
            ui = (
                <div>

                    <img className="bg" src="./assets/img/bg/shopping_bags_lite.jpg"/>

                    <Navigator title={this.state.user['name']}
                               data={context_data}
                               menu={[
                               
                               {key:"home", label:labels.menu_home, element:<Home data={context_data} key="home" onAction={this.onNavigatorAction.bind(this)}/>},
                               {key:"explore", label:labels.menu_explore, element:<Explore data={context_data} key="explore"/>}

                               ]}
                               index={index}
                    />
                </div>
            );
        }

        return ui;
    }
}

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

export default AppUser;