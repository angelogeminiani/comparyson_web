import {Dispatcher} from "flux";

let instance = new Dispatcher();

export default instance;