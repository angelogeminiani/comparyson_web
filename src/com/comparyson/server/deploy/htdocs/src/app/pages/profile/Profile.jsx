import React, {Component} from "react";
import {render} from "react-dom";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

// ------------------------------------------------------------------------
//                      c l a s s 
// ------------------------------------------------------------------------

class Profile extends Component {

    constructor(props, context) {
        super(props, context);
    }

    componentWillMount() {
        
    }

    componentDidMount() {

    }
    
    render() {
        
        return (
            <ReactCSSTransitionGroup transitionName="enter" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
                <div>{this.props.params.userId}</div>
            </ReactCSSTransitionGroup>
        )
    }
}

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

export default Profile;