// ------------------------------------------------------------------------
//                      i m p o r t
// ------------------------------------------------------------------------

import React, {Component} from "react";
import {render} from "react-dom";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import AppBar from "material-ui/AppBar";
import IconButton from "material-ui/IconButton";
import IconMenu from "material-ui/IconMenu";
import MenuItem from "material-ui/MenuItem";
import MoreVertIcon from "material-ui/svg-icons/navigation/more-vert";
//import SwipeableViews from "react-swipeable-views";

// ------------------------------------------------------------------------
//                      c l a s s 
// ------------------------------------------------------------------------

class Navigator extends Component {

    constructor(props, context) {
        super(props, context);

        console.log('1.NAVIGATOR INDEX', this.props.index);

        this.state = {
            view: this.props.menu[0].element || (<div>EMPTY NAVIGATION MENU</div>),
            index: this.props.index
        };
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {
        console.log('componentWillReceiveProps', nextProps.index);
        this.setState({
            index: nextProps.index
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('shouldComponentUpdate', nextState.index);
        return nextProps.index !== this.props.index || nextState.index !== this.state.index;
    }

    handleMenuTap(item) {
        console.log('TAP ON: ', item);
        this.setState({view: item.element, index: item.index});
    }

    onSwitchStarted({prev: current, current: next}) {
        console.log('started to switch from ${current} to ${next}');
    }

    onSwitching(progress, deck) {
        console.log('switching on progress.');
        console.log(progress, deck.state.distance);
    }

    onSwitchDone({prev, current}) {
        console.log('switch finished, current slide index: ${current}');
    }

    render() {
        const menu = this.props.menu;

        const index = this.state.index;
        //console.log('2.NAVIGATOR INDEX', index);
        /*
         let swipe_table = (
         <SwipeableViews index={index}>
         {menu.map((item, i)=> {
         item.index = i;
         return item.element
         })}
         </SwipeableViews>
         ); */


        let ui = (
            <div>
                <AppBar
                    title={this.props.title}
                    showMenuIconButton={false}
                    //iconElementLeft={<IconButton><NavigationClose /></IconButton>}
                    iconElementRight={
                    <IconMenu
                        iconButtonElement={
                          <IconButton><MoreVertIcon /></IconButton>
                        }
                        targetOrigin={{horizontal: 'right', vertical: 'top'}}
                        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                    >
                        {menu.map((item, index)=>{
                            item.index = index;
                            return <MenuItem primaryText={item.label} key={item.key} data-key={item.key} onTouchTap={(event)=>{this.handleMenuTap(item)}}/>
                        })}
                    </IconMenu>
                }
                />
                <ReactCSSTransitionGroup transitionName="fade"
                                         transitionAppear={true} transitionAppearTimeout={500}
                                         transitionEnterTimeout={500}
                                         transitionLeaveTimeout={200}>
                    {menu[index].element}
                </ReactCSSTransitionGroup>
            </div>
        );

        return ui;
    }
}

Navigator.propTypes = {
    //cardId: React.PropTypes.string,
    //user: React.PropTypes.object.isRequired
    title: React.PropTypes.string,
    data: React.PropTypes.object,
    menu: React.PropTypes.arrayOf(React.PropTypes.object),
    index: React.PropTypes.number
};
Navigator.defaultProps = {title: '', data: {}, menu: [], index: 0};

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

export default Navigator;
