// ------------------------------------------------------------------------
//                      i m p o r t s
// ------------------------------------------------------------------------

import request from "superagent";
import {API_HOST, API_TOKEN} from "../Constants";

// ------------------------------------------------------------------------
//                      c l a s s
// ------------------------------------------------------------------------

export default class BaseService {

    constructor() {

    }

    post(uri, params, callback) {
        if (!!uri) {
            params=params||{};
            params['app_token'] = API_TOKEN;
            request
                .post(API_HOST + uri)
                .send(params)
                .set('Accept', 'application/json')
                .end((err, response)=>{
                    if(!!err){
                       callback(err); 
                    } else {
                        const data = response.body;
                        if(!!data.error){
                            callback(data.error);
                        } else {
                            callback(null, data);
                        }
                    }
                });
        } else {
            callback('err_missing_uri');
        }
    }
    

}