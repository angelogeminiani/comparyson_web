// ------------------------------------------------------------------------
//                      i m p o r t 
// ------------------------------------------------------------------------

import React, {Component} from "react";
import {render} from "react-dom";
import {Router, Route, Redirect, Link, IndexRoute, hashHistory, browserHistory} from "react-router";
import {IntlProvider, FormattedMessage} from "react-intl";
import I18nStore from "./flux/i18n/I18nStore";
//-- COMPONENTS --//
import Main from "./pages/Main.jsx";
import Register from "./pages/register/Register.jsx";
import AppUser from "./pages/app_user/AppUser.jsx"

// ------------------------------------------------------------------------
//                      r o u t e s
// ------------------------------------------------------------------------

/*
 <Route path="about" component={About}/>
 <Route path="users" component={Users}>
 <Route path="/user/:userId" component={User}/>
 </Route>
 <Route path="*" component={NoMatch}/>
 */

const routes = (
    <Router history={hashHistory}>
        <Route path="/" component={Main}>
            <Route path="/register/:cardId" component={Register}/>
            <Route path="/app_user/:cardId" component={AppUser}/>
        </Route>
    </Router>
);

// ------------------------------------------------------------------------
//                      a p p
// ------------------------------------------------------------------------

class App extends Component {

    constructor() {
        super();

        this.state = {
            translations: null,
            lang: ''
        };


    }

    componentWillMount() {
        I18nStore.addChangeListener(()=> {
            this.setState(
                {
                    translations: I18nStore.getTranslations(),
                    lang: I18nStore.getLang()
                });
        });
        // initialize state
        if (!this.state.lang) {
            I18nStore.setLang(I18nStore.getLang());
        }
    }

    componentDidMount() {
        //-- remove loader --//
        //let elem = document.getElementById('loader');
        //elem.parentNode.removeChild(elem);
    }

    render() {

        let children = (<div style={{margin:'auto', textAlign: 'center'}}>...</div>);

        if (this.state.translations) {
            children = (
                <IntlProvider locale={this.state.lang} messages={this.state.translations}>
                    {routes}
                </IntlProvider>
            );

            // hide the loader
            let elem = document.getElementById('loader');
            if (!!elem) {
                elem.parentNode.removeChild(elem);
            }

        }


        return (
            children
        )
    }
}

// ------------------------------------------------------------------------
//                      e x p o r t s
// ------------------------------------------------------------------------

export default App;

