// ------------------------------------------------------------------------
//                      i m p o r t
// ------------------------------------------------------------------------

import React, {Component} from "react";
import {render} from "react-dom";
import {IntlProvider, FormattedMessage} from "react-intl";
import I18nStore from "../flux/i18n/I18nStore";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import {fade} from 'material-ui/utils/colorManipulator';
import {
    cyan500, cyan700,
    grey100, grey300, grey400, grey500,
    pinkA200, pinkA100,
    white, darkBlack, fullBlack,} from "material-ui/styles/colors";

// ------------------------------------------------------------------------
//                      t h e m e 
// ------------------------------------------------------------------------

const muiTheme = getMuiTheme({
    spacing: {
        iconSize: 24,
        desktopGutter: 24,
        desktopGutterMore: 32,
        desktopGutterLess: 16,
        desktopGutterMini: 8,
        desktopKeylineIncrement: 64,
        desktopDropDownMenuItemHeight: 32,
        desktopDropDownMenuFontSize: 15,
        desktopDrawerMenuItemHeight: 48,
        desktopSubheaderHeight: 48,
        desktopToolbarHeight: 56,
    },
    fontFamily: 'Roboto, sans-serif',
    palette: {
        primary1Color: cyan500,
        primary2Color: cyan700,
        primary3Color: grey400,
        accent1Color: pinkA200,
        accent2Color: grey100,
        accent3Color: grey500,
        textColor: darkBlack,
        alternateTextColor: white,
        canvasColor: white,
        borderColor: grey300,
        disabledColor: fade(darkBlack, 0.3),
        pickerHeaderColor: cyan500,
        clockCircleColor: fade(darkBlack, 0.07),
        shadowColor: fullBlack,
    },
});

// ------------------------------------------------------------------------
//                      c l a s s 
// ------------------------------------------------------------------------


class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lang: I18nStore.getLang()
        };
    }

    componentWillMount() {
        I18nStore.addChangeListener(()=> {
            this.setState({
                lang: I18nStore.getLang()
            });
        });
    }

    handleClick() {
        I18nStore.setLang('en');
    }

    render() {
        let sample = (
            <div>

                <h1 onClick={this.handleClick}>
                    <FormattedMessage
                        id="username"
                        defaultMessage="React Intl Translations Example"
                    />

                </h1>
                <p>{this.state.lang} </p>
                <p>{this.props.params.id}</p>

                {this.props.children}
            </div>
        );


        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                {this.props.children}
            </MuiThemeProvider>
        )
    }

}

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

export default Main;