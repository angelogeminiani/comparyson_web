import Dispatcher from "../../ApplicationDisparcher";


// ------------------------------------------------------------------------
//                      e x p o r t s
// ------------------------------------------------------------------------

export const I18N_CHANGE_LANG = 'I18N_CHANGE_LANG';

export const changeLang = (lang) => {
    const action = {
        type: I18N_CHANGE_LANG,
        lang
    };
    console.log('Dispatcher.dispatch(action): ', action);
    Dispatcher.dispatch(action);
};