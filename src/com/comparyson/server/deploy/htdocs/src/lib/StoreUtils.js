import _ from "lodash";
import {EventEmitter} from "events";

const CHANGE_EVENT = 'change';

export function createStore(spec) {
    try {
        const emitter = new EventEmitter();
        emitter.setMaxListeners(0);

        const store = Object.assign({
            emitChange() {
                emitter.emit(CHANGE_EVENT);
            },

            addChangeListener(callback) {
                emitter.on(CHANGE_EVENT, callback);
            },

            removeChangeListener(callback) {
                emitter.removeListener(CHANGE_EVENT, callback);
            }
        }, spec);

        // Auto-bind store methods for convenience
        Object.keys(store).forEach((key) => {
            if (_.isFunction(store[key])) {
                store[key] = store[key].bind(store);
            }
        });

        return store;
    } catch (err) {
        console.error(err);
        return null;
    }

}