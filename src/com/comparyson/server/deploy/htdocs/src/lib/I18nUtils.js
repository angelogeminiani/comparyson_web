import {addLocaleData} from 'react-intl';
import en from 'react-intl/locale-data/en';
import it from 'react-intl/locale-data/it';
import fr from 'react-intl/locale-data/fr';
import es from 'react-intl/locale-data/es';
import de from 'react-intl/locale-data/de';
import _ from "lodash";
import request from "superagent";


const ROOT = '/i18n/';
const DEFAULT = '/i18n/default.json';

class I18nUtilsClass {

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    constructor() {
        //super();

        //-- initialize --//
        this._init();

    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------

     getLanguage(locale) {
        const dash_index = locale.indexOf('-');
        if (dash_index >= 0) {
            return locale.substring(0, dash_index)
        }
        return locale
    }

    loadTranslations(locale, callback){
        if(_.isFunction(callback)){
            const lang = this.getLanguage(locale);
            request.get(ROOT + lang + '.json', (err, res) => {
                if(!err && !!res.body){
                     callback(res.body);
                } else {
                    // load default
                    request.get(DEFAULT, (err, res)=>{
                        callback(res.body);
                    });
                }
            });
        }
    }

    // ------------------------------------------------------------------------
    //                      p r i v a t e
    // ------------------------------------------------------------------------

    _init() {
        //-- add local data to React-Intl --//
        addLocaleData([...en, ...fr, ...es, ...it, ...de]);
    }


}

const I18nUtils = new I18nUtilsClass();

export default I18nUtils;