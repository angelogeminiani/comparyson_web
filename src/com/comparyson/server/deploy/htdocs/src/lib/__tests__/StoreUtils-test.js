jest.autoMockOff();
jest.unmock('../StoreUtils');

import {createStore} from "../StoreUtils";

console.log(createStore);

describe('i18n action & strore', ()=> {

    it('createStore', ()=> {
        let store = createStore({
            ping(){
                console.log('PING');
            }
        });

        expect(null != store).toBe(true);

        if (!!store) {
            store.addChangeListener(()=> {
                console.log('CHANGED');
            });

            store.emitChange();
        }

    });

});