package com.comparyson.server.app.bus;

/**
 * Events used in message bus
 */
public interface IEvents {


    // ------------------------------------------------------------------------
    //                      l o g i n
    // ------------------------------------------------------------------------

    String ETAG_LOGIN = "tag_login";

    String EVENT_SIGNUP = "event_signup";
    String EVENT_RESET_PASSWORD = "reset_password";
    String EVENT_CONFIRMED_ACCOUNT = "confirmed_account";
    String EVENT_ASK_RESET_PASSWORD = "ask_reset_password";
    String EVENT_ASK_CONFIRM_ACCOUNT = "ask_confirm_account";



}
