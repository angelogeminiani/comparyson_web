package com.comparyson.server.app.bus;


import org.lyj.commons.event.Event;
import org.lyj.commons.event.bus.MessageBus;
import org.lyj.commons.logging.AbstractLogEmitter;


/**
 * Main message listeners.
 */
public class MessageListener
        extends AbstractLogEmitter {

    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    private MessageListener() {
        MessageBus.getInstance()
                .createListener()
                .on(this::handle);

    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    //                      p r i v a t e
    // ------------------------------------------------------------------------


    private void handle(final Event event) {
        final String name = event.getName();
        final String tag = event.getTag();
        if (IEvents.ETAG_LOGIN.equals(tag)) {

            if (IEvents.EVENT_SIGNUP.equals(name)) {
                this.onSignUp(event);
            } else if (IEvents.EVENT_ASK_RESET_PASSWORD.equals(name)) {
                this.onAskResetPassword(event);
            } else if (IEvents.EVENT_ASK_CONFIRM_ACCOUNT.equals(name)) {
                this.onAskConfirmAccount(event);
            } else if (IEvents.EVENT_RESET_PASSWORD.equals(name)) {
                this.onResetPassword(event);
            } else if (IEvents.EVENT_CONFIRMED_ACCOUNT.equals(name)) {
                this.onConfirmedAccount(event);
            }

        }

    }


    // ------------------------------------------------------------------------
    //                      l o g i n
    // ------------------------------------------------------------------------

    private void onSignUp(final Event event) {

    }

    private void onAskResetPassword(final Event event) {

    }

    private void onAskConfirmAccount(final Event event) {

    }

    private void onResetPassword(final Event event) {

    }

    private void onConfirmedAccount(final Event event) {

    }

    // ------------------------------------------------------------------------
    //                      S T A T I C
    // ------------------------------------------------------------------------

    private static MessageListener __instance;

    public static MessageListener getInstance() {
        if (null == __instance) {
            __instance = new MessageListener();
        }
        return __instance;
    }

}
