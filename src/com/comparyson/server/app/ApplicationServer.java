package com.comparyson.server.app;

import com.comparyson.server.app.bus.MessageListener;
import com.comparyson.server.app.webserver.model.schema.SchemaUser;
import com.comparyson.server.deploy.config.ConfigHelper;
import com.comparyson.server.app.webserver.ApiServer;
import com.comparyson.server.app.webserver.WebServer;
import org.bson.Document;
import org.lyj.Lyj;
import org.lyj.commons.io.jsonrepository.JsonRepository;
import org.lyj.commons.logging.Logger;
import org.lyj.commons.logging.util.LoggingUtils;
import org.lyj.commons.util.FormatUtils;
import org.lyj.commons.util.LocaleUtils;
import org.lyj.ext.mongo.LyjMongo;
import org.lyj.ext.mongo.utils.LyjMongoObjects;

import java.util.*;

/**
 * Application Server
 */
public class ApplicationServer {


    // ------------------------------------------------------------------------
    //                      C O N S T
    // ------------------------------------------------------------------------

    public static final String VERSION = "1.0.1";

    private static final String CHANNEL_MAIN = "main";

    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------

    private boolean _test_mode;
    private final JsonRepository _config;
    private ApiServer _api_server;
    private WebServer _web_server;

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    public ApplicationServer(final boolean test_mode) {

        LocaleUtils.setCurrent(Locale.ENGLISH);

        _test_mode = test_mode;
        _config = Lyj.getConfiguration();

        this.getLogger().info("STARTING SERVER VERSION ".concat(VERSION));
    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------


    public void start() throws Exception {
        this.getLogger().info("APP SERVER: INITIALIZING...");
        this.init();
    }

    public void stop() {
        if (null != _web_server) {
            _web_server.stop();
        }
        if (null != _api_server) {
            _api_server.stop();
        }
    }

    // ------------------------------------------------------------------------
    //                      p r i v a t e
    // ------------------------------------------------------------------------

    private Logger getLogger() {
        return LoggingUtils.getLogger(this);
    }

    private void init() {
        try {
            // init global message listener
            this.getLogger().info("APP SERVER: INITIALIZING GLOBAL LISTENER");
            MessageListener.getInstance();
            this.getLogger().info("APP SERVER: GLOBAL LISTENER INITIALIZED");


            // DATABASE
            this.initDatabase();

            if (!_test_mode) {
                // tiny web server
                final boolean api_enabled = ConfigHelper.instance().apiEnabled();
                this.getLogger().info("APP SERVER: API SERVER IS " + (api_enabled ? "ENABLED" : "DISABLED"));
                if (api_enabled) {
                    _api_server = new ApiServer().start();
                }

                final boolean web_enabled = ConfigHelper.instance().webEnabled();
                this.getLogger().info("APP SERVER: WEB SERVER IS " + (web_enabled ? "ENABLED" : "DISABLED"));
                if (web_enabled) {
                    _web_server = new WebServer().start();
                }

                // SCHEDULED JOBS
                if(ConfigHelper.instance().enableTask()){
                    this.getLogger().info("TASK: ENABLED");
                    this.initScheduledJobs();
                } else {
                    this.getLogger().warning("TASK: DISABLED");
                }
            }

        } catch (Throwable t) {
            this.getLogger().error("Error initializing server", t);
        }
    }

    private void initDatabase() {
        // init database configuration
        this.getLogger().info("DATABASE: INITIALIZING CONFIGURATION");
        LyjMongo.getInstance().init(_config.get("databases"));
        //if(true) return;


        // init schema (async)
        //Async.invoke((args) -> {

        this.getLogger().info("DATABASE: INITIALIZING SCHEMA");
        //-- SCHEMAS --//
        LyjMongo.getInstance().getSchemas().register(new SchemaUser());


        this.getLogger().info("DATABASE: CHECK INDEXES");
        //-- INDEXES --//
        final Map<String, List<Document>> index_report = LyjMongo.getInstance().getSchemas().ensureIndexes();
        // log report
        if (index_report.size() > 0) {
            this.getLogger().info("Start Updating Index -----------------");
            final Set<String> collections = index_report.keySet();
            for (final String collectionName : collections) {
                final List<Document> collectionReport = index_report.get(collectionName);
                final String message = "\t".concat(collectionName).concat(FormatUtils.format("(%s)", collectionReport.size()))
                        .concat(": ").concat(LyjMongoObjects.toJson(collectionReport));
                this.getLogger().info(message);
            }
            this.getLogger().info("End Updating Index   -----------------");
        }


        //});
    }

    private void initScheduledJobs() {


        // franky
        //ScheduledBot.start();
    }



}
