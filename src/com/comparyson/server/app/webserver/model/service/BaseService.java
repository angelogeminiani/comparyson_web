package com.comparyson.server.app.webserver.model.service;

import com.comparyson.server.app.IConstants;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.lyj.commons.Delegates;
import org.lyj.ext.mongo.service.AbstractService;

/**
 * Services superclass
 */
public abstract class BaseService
        extends AbstractService
        implements IConstants {

    // ------------------------------------------------------------------------
    //                      C O N S T
    // ------------------------------------------------------------------------

    public static final String DATE_LONG = IConstants.DATE_LONG;

    private static final String CONNECTION = "funnygain";
    private static final String DATABASE = "funnygain";

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------

    public String getConnectionName() {
        return CONNECTION;
    }

    public String getDatabaseName() {
        return DATABASE;
    }

    // ------------------------------------------------------------------------
    //                      p r o t e c t e d
    // ------------------------------------------------------------------------

    protected void findLast(final String collection_name, final Bson filter,
                            final Delegates.SingleResultCallback<Document> callback) {
        try{
            final Document response = this.findLast(collection_name, filter);
            Delegates.invoke(callback, null, response);
        }catch(Throwable t){
            Delegates.invoke(callback, t, null);
        }
    }

    protected Document findLast(final String collection_name, final Bson filter) throws Exception {
        final MongoCollection<Document> collection = super.getCollection(collection_name);
        com.mongodb.client.FindIterable<Document> iterable = collection.find(filter).limit(1).sort(new Document(DATE_LONG, -1));
        return iterable.first();
    }

}
