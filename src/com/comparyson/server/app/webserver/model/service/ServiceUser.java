package com.comparyson.server.app.webserver.model.service;

import com.comparyson.server.app.webserver.model.model.ModelUser;
import com.comparyson.server.app.bus.IEvents;
import com.comparyson.server.app.webserver.model.schema.SchemaUser;
import com.comparyson.server.app.webserver.templates.Templates;
import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.lyj.commons.Delegates;
import org.lyj.commons.event.Event;
import org.lyj.commons.event.bus.MessageBus;
import org.lyj.commons.lang.Counter;
import org.lyj.commons.util.*;
import org.lyj.ext.mongo.utils.LyjMongoObjects;

import java.util.*;

import static com.mongodb.client.model.Filters.eq;


/**
 * User SERVICE
 */
public class ServiceUser extends BaseService {

    // ------------------------------------------------------------------------
    //                      C O N S T
    // ------------------------------------------------------------------------

    private static final String COLLECTION = SchemaUser.COLLECTION;

    private static final String UID = SchemaUser.UID;
    private static final String EMAIL = SchemaUser.EMAIL;
    private static final String PASSWORD = SchemaUser.PASSWORD;
    private static final String LANG = SchemaUser.LANG;
    private static final String AGE = SchemaUser.AGE;
    private static final String BIRTH_DATE = SchemaUser.BIRTH_DATE;
    private static final String BIRTH_DATE_DAY = SchemaUser.BIRTH_DATE_DAY;
    private static final String BIRTH_DATE_MONTH = SchemaUser.BIRTH_DATE_MONTH;
    private static final String BIRTH_DATE_YEAR = SchemaUser.BIRTH_DATE_YEAR;
    private static final String IS_MODERATOR = SchemaUser.IS_MODERATOR;
    private static final String MOBILE = SchemaUser.MOBILE;

    private static final String FACEBOOK_ID = SchemaUser.FACEBOOK_ID;
    private static final String GOOGLE_PLAYER_ID = SchemaUser.GOOGLE_PLAYER_ID;
    private static final String APPLE_PLAYER_ID = SchemaUser.APPLE_PLAYER_ID;

    private static final String PARENT_UID = SchemaUser.PARENT_UID;
    private static final String ACCOUNT_CONFIRMED = SchemaUser.ACCOUNT_CONFIRMED;

    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    public ServiceUser() {

    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------

    public void find(final Bson filter, final Delegates.SingleResultCallback<List<Document>> callback) {
        super.findAsync(COLLECTION, filter, callback);
    }

    public void findById(final String id, final Delegates.SingleResultCallback<Document> callback) {
        super.findByIdAsync(COLLECTION, id, callback);
    }

    public Document findById(final String id) throws Exception {
        return super.findById(COLLECTION, id);
    }

    public Document findByEmail(final String email) throws Exception {
        return super.findOne(COLLECTION, eq(EMAIL, email));
    }

    public Document findByUid(final String uid) throws Exception {
        return super.findOne(COLLECTION, new Document(UID, uid));
    }

    public Document findByMobile(final String mobile) throws Exception {
        return super.findOne(COLLECTION, new Document(MOBILE, mobile));
    }

    public List<String> findIds(final Bson filter,
                                final int skip, final int limit, final Bson sort) throws Exception {
        return super.findIds(COLLECTION, filter, skip, limit, sort);
    }

    public List<String> findIds(final Bson filter) throws Exception {
        return super.findIds(COLLECTION, filter, 0, 0, null);
    }


    public FindIterable<Document> findIterable() throws Exception {
        return this.findIterable(new Document(), 0, 0, null, null);
    }

    public FindIterable<Document> findIterable(final Bson filter,
                                               final int skip, final int limit,
                                               final Bson sort,
                                               final Bson projection) throws Exception {
        return super.findIterable(COLLECTION, filter, skip, limit, sort, projection);
    }

    public boolean exists(final String id) throws Exception {
        return super.exists(COLLECTION, id);
    }

    public Document remove(final String id) throws Exception {
        final Document item = super.remove(COLLECTION, id);
        if (null != item) {
            // remove extra documents
            removeDependencies(id);
        }
        return item;
    }

    public Document insert(final Document item) throws Exception {
        return super.insert(COLLECTION, item);
    }

    public Document upsert(final Document item) throws Exception {
        return super.upsert(COLLECTION, item);
    }

    public void updateField(final String id, final String field_id, final Object field_value,
                            final Delegates.SingleResultCallback<Document> callback) {
        try {
            final Document response = this.updateField(id, field_id, field_value);
            Delegates.invoke(callback, null, response);
        } catch (Throwable t) {
            Delegates.invoke(callback, t, null);
            super.error("updateField", t);
        }
    }

    public Document updateField(final String id, final String field_id, final Object field_value) throws Exception {
        final ModelUser user = ServiceUser.wrapById(id);
        if (null != user) {
            final Document data = new Document(field_id, field_value);
            if (AGE.equals(field_id)) {
                final Date date = DateUtils.postpone(DateUtils.now(), DateUtils.YEAR, -1 * ConversionUtils.toInteger(field_value));
                final Document birth_date = new Document();
                birth_date.put(BIRTH_DATE_DAY, DateUtils.getDayOfMonth(date));
                birth_date.put(BIRTH_DATE_MONTH, DateUtils.getMonth(date));
                birth_date.put(BIRTH_DATE_YEAR, DateUtils.getYear(date));

                data.put(BIRTH_DATE, birth_date.toString());
            } else if (BIRTH_DATE.equals(field_id)) {
                final JsonWrapper w = new JsonWrapper(field_value);
                final Date date = DateUtils.encodeDateTime(w.getInt(BIRTH_DATE_YEAR), w.getInt(BIRTH_DATE_MONTH), w.getInt(BIRTH_DATE_DAY), 0, 0, 0);

                data.put(AGE, DateUtils.dateDiff(DateUtils.now(), date, DateUtils.YEAR));
            } else if (PASSWORD.equals(field_id)) {
                data.put(ACCOUNT_CONFIRMED, false); // reset account when user change email
                this.sendAskConfirmAccountEvent(user);
            }
            return this.update(id, data);
        } else {
            throw new Exception(ERR_USER_NOT_EXISTS);
        }
    }

    public Document update(final String id, final Document data) throws Exception {
        return super.update(COLLECTION, id, data);
    }

    public void getFieldValue(final String id, final String fieldName,
                              final Delegates.SingleResultCallback<Object> callback) {
        super.findOneAsync(COLLECTION, new Document(ID, id), LyjMongoObjects.asProjection(fieldName), (err, document) -> {
            if (null != err) {
                Delegates.invoke(callback, err, null);
            } else {
                if (null != document) {
                    Delegates.invoke(callback, null, document.get(fieldName));
                } else {
                    Delegates.invoke(callback, null, null);
                }
            }
        });
    }

    public Document lookup(final String email,
                           final String facebook_id,
                           final String google_player_id,
                           final String apple_player_id) throws Exception {
        Document response = null;
        if (StringUtils.hasText(email)) {
            response = this.findByEmail(email);
        }

        if (null == response && StringUtils.hasText(facebook_id)) {
            response = this.findOne(COLLECTION, new Document().append(FACEBOOK_ID, facebook_id));
        }
        if (null == response && StringUtils.hasText(google_player_id)) {
            response = this.findOne(COLLECTION, new Document().append(GOOGLE_PLAYER_ID, google_player_id));
        }
        if (null == response && StringUtils.hasText(apple_player_id)) {
            response = this.findOne(COLLECTION, new Document().append(APPLE_PLAYER_ID, apple_player_id));
        }

        return response;
    }

    public long count() throws Exception {
        return super.count(COLLECTION, new Document());
    }

    public List<Document> getByParentUid(final String raw_parent_uid,
                                         final boolean onlyConfirmed,
                                         final int max_level) throws Exception {
        final String parent_uid = ModelUser.sanitizeUid(raw_parent_uid);
        final List<Document> response = new ArrayList<>();
        this.getByParentUid(parent_uid, onlyConfirmed, response, 0, max_level);
        return response;
    }

    public long countByParentUid(final String raw_parent_uid,
                                 final boolean onlyConfirmed,
                                 final int max_level) throws Exception {
        final String parent_uid = ModelUser.sanitizeUid(raw_parent_uid);
        final Counter counter = new Counter();
        this.countByParentUid(parent_uid, onlyConfirmed, counter, 0, max_level);
        return counter.value();
    }

    public Document signin(final String email, final String password) throws Exception {

        final String md5_psw = super.toMD5(password);
        final Document filter = new Document();
        filter.put(EMAIL, email);
        filter.put(PASSWORD, md5_psw);
        final Document user = super.findOne(COLLECTION, filter);
        if (null == user) {
            throw new Exception(ERR_WRONG_LOGIN);
        } else {
            return user;
        }
    }

    public Document signinFacebook(final String facebookId) throws Exception {

        final Document filter = new Document();
        filter.put(FACEBOOK_ID, facebookId);
        final Document user = super.findOne(COLLECTION, filter);
        if (null == user) {
            throw new Exception(ERR_WRONG_LOGIN);
        } else {
            return user;
        }
    }

    public Document signinGooglePlayer(final String googlePlayerId) throws Exception {

        final Document filter = new Document();
        filter.put(GOOGLE_PLAYER_ID, googlePlayerId);
        final Document user = super.findOne(COLLECTION, filter);
        if (null == user) {
            throw new Exception(ERR_WRONG_LOGIN);
        } else {
            return user;
        }
    }

    public Document signinApplePlayer(final String applePlayerId) throws Exception {

        final Document filter = new Document();
        filter.put(APPLE_PLAYER_ID, applePlayerId);
        final Document user = super.findOne(COLLECTION, filter);
        if (null == user) {
            throw new Exception(ERR_WRONG_LOGIN);
        } else {
            return user;
        }
    }

    public Document signup(final String email,
                           final String password,
                           final String lang,
                           final String facebook_id,
                           final String google_player_id,
                           final String apple_player_id,
                           final String raw_parent_uid) throws Exception {
        final String md5_psw = super.toMD5(password);
        // check existing
        final Document item = this.lookup(email, facebook_id, google_player_id, apple_player_id);
        if (null == item) {
            // check parent user
            final String parent_uid = ModelUser.sanitizeUid(raw_parent_uid);
            final ModelUser parent = StringUtils.hasText(parent_uid) ? wrapByUid(parent_uid) : null;

            // ok, user does not exists
            final ModelUser new_user = new ModelUser();
            new_user.setId(super.toMD5(email));
            new_user.setParentUid(null != parent ? parent.getUid() : "");
            new_user.setLang(LocaleUtils.getLocale(lang).getLanguage());
            new_user.setEmail(email);
            new_user.setPassword(md5_psw);
            new_user.setFacebookId(facebook_id);
            new_user.setGooglePlayerId(google_player_id);
            new_user.setApplePlayerId(apple_player_id);

            final Document response = this.insert(new_user.document());

            // send event
            new_user.setPassword(password); // clear password
            this.sendSignUpEvent(new_user);
            this.sendAskConfirmAccountEvent(new_user);

            return response;
        } else {
            // error, user exists
            throw new Exception(ERR_USER_EXISTS);
        }
    }

    public Document askResetPassword(final String email,
                                     final String password) throws Exception {
        final ModelUser user = wrapByEmail(email);
        if (null != user) {
            final Document response = Document.parse(user.document().toJson()); // clone

            // send event
            user.setPassword(password);
            this.sendAskResetPasswordEvent(user);

            return response;
        } else {
            throw new Exception(ERR_USER_NOT_EXISTS);
        }
    }

    public Document askConfirmAccount(final String userId) throws Exception {
        final ModelUser user = wrapById(userId);
        if (null != user) {

            this.sendAskConfirmAccountEvent(user);

            return user.document();
        } else {
            throw new Exception(ERR_USER_NOT_EXISTS);
        }
    }

    /**
     * Reset password with one passed by user or with one generated by system
     *
     * @param user_id      ID of User
     * @param new_password (Optional) if empty, new password is generated from system
     * @return User
     */
    public Document resetPassword(final String user_id, final String new_password) throws Exception {
        final String password = StringUtils.hasText(new_password) ? new_password : RandomUtils.randomAlphanumeric(6).toLowerCase();
        final ModelUser user = wrapById(user_id);
        if (null != user) {
            final String md5_psw = super.toMD5(password);
            final Document response = this.update(user_id, new Document(PASSWORD, md5_psw));

            // send event
            user.setPassword(password);
            this.sendResetPasswordEvent(user);

            return response;
        } else {
            throw new Exception(ERR_USER_NOT_EXISTS);
        }
    }

    public String resetPasswordHTML(final String user_id, final String new_password) {
        try {
            final Document raw_user = this.resetPassword(user_id, new_password);
            if (null == raw_user) {
                // load error template
                return Templates.instance().getTemplateHTML(LocaleUtils.getLocale(Locale.ENGLISH).getLanguage(),
                        Templates.TPL_ERROR, MapBuilder.create(String.class, String.class)
                                .append(Templates.ERROR, "").toMap());
            } else {
                final ModelUser user = new ModelUser(raw_user);

                final Map<String, String> model = MapBuilder.create(String.class, String.class)
                        .append(Templates.USER_NAME, user.lookUpName())
                        .append(Templates.EMAIL, user.getEmail())
                        .append(Templates.PASSWORD, new_password)
                        .toMap();

                // send email
                return Templates.instance().getTemplate(user.getLang(),
                        Templates.TPL_CHANGED_PASSWORD, Templates.TYPE_HTML, model);
            }
        } catch (Throwable t) {
            return Templates.instance().getTemplateHTML(LocaleUtils.getLocale(Locale.ENGLISH).getLanguage(),
                    Templates.TPL_ERROR, MapBuilder.create(String.class, String.class)
                            .append(Templates.ERROR, t.toString()).toMap());
        }
    }

    public Document confirmAccount(final String user_id,
                                   final String email) throws Exception {
        final ModelUser user = wrapById(user_id);
        if (null != user) {
            final Document response = this.update(user_id, new Document(ACCOUNT_CONFIRMED, true));

            // send event
            user.setAccountConfirmed(true);
            this.sendAccountConfirmedEvent(user);

            return response;
        } else {
            throw new Exception(ERR_USER_NOT_EXISTS);
        }
    }

    public String confirmAccountHTML(final String user_id, final String email) {
        try {
            final Document raw_user = this.confirmAccount(user_id, email);
            if (null == raw_user) {
                // load error template
                return Templates.instance().getTemplateHTML(LocaleUtils.getLocale(Locale.ENGLISH).getLanguage(),
                        Templates.TPL_ERROR, MapBuilder.create(String.class, String.class)
                                .append(Templates.ERROR, "").toMap());
            } else {
                final ModelUser user = new ModelUser(raw_user);

                final Map<String, String> model = MapBuilder.create(String.class, String.class)
                        .append(Templates.USER_NAME, user.lookUpName())
                        .append(Templates.EMAIL, user.getEmail())
                        .toMap();

                // send email
                return Templates.instance().getTemplate(user.getLang(),
                        Templates.TPL_CONFIRMED_ACCOUNT, Templates.TYPE_HTML, model);
            }
        } catch (Throwable t) {
            return Templates.instance().getTemplateHTML(LocaleUtils.getLocale(Locale.ENGLISH).getLanguage(),
                    Templates.TPL_ERROR, MapBuilder.create(String.class, String.class)
                            .append(Templates.ERROR, t.toString()).toMap());
        }
    }

    // ------------------------------------------------------------------------
    //                      p r i v a t e
    // ------------------------------------------------------------------------

    private void sendSignUpEvent(final ModelUser user) {
        MessageBus.getInstance().emit(Event.create(this, IEvents.EVENT_SIGNUP, user).setTag(IEvents.ETAG_LOGIN));
    }

    private void sendAskResetPasswordEvent(final ModelUser user) {
        MessageBus.getInstance().emit(Event.create(this, IEvents.EVENT_ASK_RESET_PASSWORD, user).setTag(IEvents.ETAG_LOGIN));
    }

    private void sendAskConfirmAccountEvent(final ModelUser user) {
        MessageBus.getInstance().emit(Event.create(this, IEvents.EVENT_ASK_CONFIRM_ACCOUNT, user).setTag(IEvents.ETAG_LOGIN));
    }

    private void sendResetPasswordEvent(final ModelUser user) {
        MessageBus.getInstance().emit(Event.create(this, IEvents.EVENT_RESET_PASSWORD, user).setTag(IEvents.ETAG_LOGIN));
    }

    private void sendAccountConfirmedEvent(final ModelUser user) {
        MessageBus.getInstance().emit(Event.create(this, IEvents.EVENT_CONFIRMED_ACCOUNT, user).setTag(IEvents.ETAG_LOGIN));
    }

    /**
     * Remove all user dependencies from other collections
     *
     * @param id User ID
     */

    private void removeDependencies(final String id)
            throws Exception {


    }

    private void getByParentUid(final String parent_uid,
                                final boolean onlyConfirmed,
                                final List<Document> list,
                                final int level,
                                final int max_level) throws Exception {
        final Document filter = new Document(PARENT_UID, parent_uid);
        if (onlyConfirmed) {
            filter.append(ACCOUNT_CONFIRMED, true);
        }
        final Bson projection = Projections.exclude(PASSWORD, FACEBOOK_ID, GOOGLE_PLAYER_ID, APPLE_PLAYER_ID);
        final List<Document> result = super.find(COLLECTION, filter, 0, 0, null, projection);
        if (!CollectionUtils.isEmpty(result)) {
            list.addAll(result);
            // continue deep
            if (level + 1 <= max_level || max_level < 1) {
                for (final Document item : result) {
                    final String uid = new ModelUser(item).getUid();
                    if (StringUtils.hasText(uid)) {
                        this.getByParentUid(uid, onlyConfirmed, list, level + 1, max_level);
                    }
                }
            }
        }
    }

    private void countByParentUid(final String parent_uid,
                                  final boolean onlyConfirmed,
                                  final Counter counter,
                                  final int level,
                                  final int max_level) throws Exception {
        final Document filter = new Document(PARENT_UID, parent_uid);
        if (onlyConfirmed) {
            filter.append(ACCOUNT_CONFIRMED, true);
        }
        final Bson projection = Projections.include(UID);
        final List<Document> result = super.find(COLLECTION, filter, 0, 0, null, projection);
        if (!CollectionUtils.isEmpty(result)) {
            counter.inc(result.size());
            // continue deep
            if (level + 1 <= max_level || max_level < 1) {
                for (final Document item : result) {
                    final String uid = item.getString(UID);
                    if (StringUtils.hasText(uid)) {
                        this.countByParentUid(uid, onlyConfirmed, counter, level + 1, max_level);
                    }
                }
            }
        }
    }

    // ------------------------------------------------------------------------
    //                      C O N S T
    // ------------------------------------------------------------------------

    private static ServiceUser __instance;

    public static ServiceUser instance() {
        if (null == __instance) {
            __instance = new ServiceUser();
        }
        return __instance;
    }

    public static ModelUser wrapByEmail(final String email) throws Exception {
        final Document raw = instance().findByEmail(email);
        if (null != raw) {
            return new ModelUser(raw);
        }
        return null;
    }

    public static ModelUser wrapByMobile(final String mobile) throws Exception {
        final Document raw = instance().findByMobile(mobile);
        if (null != raw) {
            return new ModelUser(raw);
        }
        return null;
    }

    public static ModelUser wrapById(final String id) throws Exception {
        final Document raw = instance().findById(id);
        if (null != raw) {
            return new ModelUser(raw);
        }
        return null;
    }

    public static ModelUser wrapByUid(final String uid) throws Exception {
        final Document raw = instance().findByUid(uid);
        if (null != raw) {
            return new ModelUser(raw);
        }
        return null;
    }


    public static void wrapById(final String id, final Delegates.SingleResultCallback<ModelUser> callback) {
        try {
            Delegates.invoke(callback, null, wrapById(id));
        } catch (Throwable t) {
            Delegates.invoke(callback, t, null);
        }
    }

}
