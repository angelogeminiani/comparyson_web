package com.comparyson.server.app.webserver.model.schema;

import com.comparyson.server.app.IConstants;
import org.lyj.ext.mongo.schema.AbstractSchema;

/**
 * Services superclass
 */
public abstract class BaseSchema
        extends AbstractSchema
        implements IConstants {

    // ------------------------------------------------------------------------
    //                      C O N S T
    // ------------------------------------------------------------------------

    private static final String CONNECTION = "funnygain";
    private static final String DATABASE = "funnygain";

    public static final String KEYWORDS = "keywords";

    // ------------------------------------------------------------------------
    //                      a b s t r a c t
    // ------------------------------------------------------------------------

    public abstract String getCollectionName();

    // ------------------------------------------------------------------------
    //                      p r o t e c t e d
    // ------------------------------------------------------------------------

    public String getConnectionName() {
        return CONNECTION;
    }

    public String getDatabaseName() {
        return DATABASE;
    }




}
