package com.comparyson.server.app.webserver.model.schema;

import com.comparyson.server.app.webserver.utils.DefaultUtils;
import com.comparyson.server.app.IConstants;
import org.bson.Document;
import org.lyj.ext.mongo.schema.LyjMongoField;

import java.util.List;

/**
 * Schema for User
 *
 */
public final class SchemaUser
        extends BaseSchema {

    // ------------------------------------------------------------------------
    //                      C O N S T
    // ------------------------------------------------------------------------

    public static final String COLLECTION = "users";

    public static final String UID = "uid";
    public static final String DATE_LONG = IConstants.DATE_LONG;
    public static final String DATE_ARRAY = "date_array";
    public static final String DATE_TEXT = "date_text";
    public static final String DATE_WEEK = "date_week";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String PIN = "pin";
    public static final String LANG = "lang";
    public static final String AGE = "age";
    public static final String GENDER = "gender";
    public static final String BIRTH_DATE = "birth_date";
    public static final String BIRTH_DATE_DAY = "day";
    public static final String BIRTH_DATE_MONTH = "month";
    public static final String BIRTH_DATE_YEAR = "year";
    public static final String IS_MODERATOR = "is_moderator";

    public static final String FACEBOOK_ID = "facebook_id";
    public static final String GOOGLE_PLAYER_ID = "google_player_id";
    public static final String APPLE_PLAYER_ID = "apple_player_id";

    public static final String PARENT_UID = "parent_uid"; // uid code of PR

    public static final String NAME = "name";
    public static final String MOTTO = "motto";
    public static final String ALIAS = "alias";
    public static final String AVATAR = "avatar";
    public static final String MOBILE = "mobile";
    public static final String ADDRESS = "address";
    public static final String ADDRESS_ZIP = "address_zip";
    public static final String ADDRESS_CITY = "address_city";
    public static final String ADDRESS_STATE = "address_state";

    public static final String ACCOUNT_CONFIRMED = "account_confirmed";

    public static final String BADGES = "badges";


    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    public SchemaUser(){
        this.init();
    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------

    public String getCollectionName() {
        return COLLECTION;
    }

    // ------------------------------------------------------------------------
    //                      p r i v a t e
    // ------------------------------------------------------------------------

    private void init(){
        // fields
        super.field(new LyjMongoField(DATE_LONG, Long.class, true, false)).setDefault(DefaultUtils::getDateLong);
        super.field(new LyjMongoField(DATE_ARRAY, List.class, false, false)).setDefault(DefaultUtils::getDateList);
        super.field(new LyjMongoField(DATE_TEXT, String.class, false, false)).setDefault(DefaultUtils::getDateString);
        super.field(new LyjMongoField(DATE_WEEK, Integer.class, false, false)).setDefault(DefaultUtils::getDateWeek);

        super.field(new LyjMongoField(UID, String.class, true, false)).setDefault("");
        super.field(new LyjMongoField(PARENT_UID, String.class, true, false)).setDefault("");
        super.field(new LyjMongoField(IS_MODERATOR, Boolean.class, true, false));
        super.field(new LyjMongoField(EMAIL, String.class, true, false));

        super.field(new LyjMongoField(FACEBOOK_ID, String.class, true, false));
        super.field(new LyjMongoField(GOOGLE_PLAYER_ID, String.class, true, false));
        super.field(new LyjMongoField(APPLE_PLAYER_ID, String.class, true, false));

        super.field(new LyjMongoField(NAME, String.class, false, false));
        super.field(new LyjMongoField(MOTTO, String.class, false, false)).setDefault(DefaultUtils::rndMotto);
        super.field(new LyjMongoField(ALIAS, String.class, false, false));
        super.field(new LyjMongoField(AVATAR, String.class, false, false)).setDefault(DefaultUtils::rndAvatar);
        super.field(new LyjMongoField(GENDER, String.class, false, false)).setDefault("m");
        super.field(new LyjMongoField(AGE, Integer.class, false, false)).setDefault(18);
        super.field(new LyjMongoField(MOBILE, String.class, true, false)).setDefault(""); // used to identify user
        super.field(new LyjMongoField(PIN, String.class, true, false)).setDefault(DefaultUtils::rndPin); // used to identify user
        super.field(new LyjMongoField(ADDRESS, String.class, false, false)).setDefault("");
        super.field(new LyjMongoField(ADDRESS_CITY, String.class, false, false)).setDefault("");
        super.field(new LyjMongoField(ADDRESS_STATE, String.class, false, false)).setDefault("");
        super.field(new LyjMongoField(ADDRESS_ZIP, String.class, false, false)).setDefault("");

        super.field(new LyjMongoField(ACCOUNT_CONFIRMED, Boolean.class, true, false)).setDefault(false);

        super.field(new LyjMongoField(BADGES, List.class, true, false));

        // compound indexes
        super.index(new Document(EMAIL, 1).append(PASSWORD, 1), true);
        super.index(new Document(MOBILE, 1).append(PIN, 1), false);

    }

}
