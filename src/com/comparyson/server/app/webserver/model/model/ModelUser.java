package com.comparyson.server.app.webserver.model.model;

import com.comparyson.server.app.webserver.model.schema.SchemaUser;
import org.bson.Document;
import org.lyj.Lyj;
import org.lyj.commons.util.MapBuilder;
import org.lyj.commons.util.PhoneUtils;
import org.lyj.commons.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 *
 */
public class ModelUser
        extends BaseModel {

    public static final String UID = SchemaUser.UID;
    public static final String EMAIL = SchemaUser.EMAIL;
    public static final String PASSWORD = SchemaUser.PASSWORD;
    public static final String LANG = SchemaUser.LANG;
    public static final String AGE = SchemaUser.AGE;
    public static final String GENDER = SchemaUser.GENDER;

    public static final String FACEBOOK_ID = SchemaUser.FACEBOOK_ID;
    public static final String GOOGLE_PLAYER_ID = SchemaUser.GOOGLE_PLAYER_ID;
    public static final String APPLE_PLAYER_ID = SchemaUser.APPLE_PLAYER_ID;

    public static final String PARENT_UID = SchemaUser.PARENT_UID;

    public static final String BIRTH_DATE = SchemaUser.BIRTH_DATE;
    public static final String BIRTH_DATE_DAY = SchemaUser.BIRTH_DATE_DAY;
    public static final String BIRTH_DATE_MONTH = SchemaUser.BIRTH_DATE_MONTH;
    public static final String BIRTH_DATE_YEAR = SchemaUser.BIRTH_DATE_YEAR;

    public static final String IS_MODERATOR = SchemaUser.IS_MODERATOR;

    public static final String NAME = SchemaUser.NAME;
    public static final String ALIAS = SchemaUser.ALIAS;
    public static final String MOTTO = SchemaUser.MOTTO;
    public static final String AVATAR = SchemaUser.AVATAR;
    public static final String MOBILE = SchemaUser.MOBILE;
    public static final String PIN = SchemaUser.PIN;
    public static final String ADDRESS = SchemaUser.ADDRESS;
    public static final String ADDRESS_CITY = SchemaUser.ADDRESS_CITY;
    public static final String ADDRESS_STATE = SchemaUser.ADDRESS_STATE;
    public static final String ADDRESS_ZIP = SchemaUser.ADDRESS_ZIP;

    public static final String ACCOUNT_CONFIRMED = SchemaUser.ACCOUNT_CONFIRMED;

    public static final String BADGES = SchemaUser.BADGES;


    //-- UID ALIAS --//

    private static final Map<String, String> UID_ALIAS = MapBuilder.createSS()
            .append("lsr", "leisorm")
            .toMap();

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    public ModelUser() {
        super(SchemaUser.class);
    }

    public ModelUser(final Document document) {
        super(SchemaUser.class, document);
        this.init();
    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------

    public String getUid() {
        return super.getString(UID);
    }

    public void setUid(final String value) {
        this.document().put(UID, value);
    }

    public String getParentUid() {
        return super.getString(PARENT_UID);
    }

    public void setParentUid(final String value) {
        this.document().put(PARENT_UID, sanitizeUid(value));
    }

    public String getFacebookId() {
        return super.getString(FACEBOOK_ID);
    }

    public void setFacebookId(final String value) {
        this.document().put(FACEBOOK_ID, value);
    }

    public String getGooglePlayerId() {
        return super.getString(GOOGLE_PLAYER_ID);
    }

    public void setGooglePlayerId(final String value) {
        this.document().put(GOOGLE_PLAYER_ID, value);
    }

    public String getApplePlayerId() {
        return super.getString(APPLE_PLAYER_ID);
    }

    public void setApplePlayerId(final String value) {
        this.document().put(APPLE_PLAYER_ID, value);
    }

    public String getLang() {
        return super.getString(LANG, Lyj.getLang());
    }

    public void setLang(final String value) {
        this.document().put(LANG, value);
    }

    public String getEmail() {
        return super.getString(EMAIL);
    }

    public void setEmail(final String value) {
        this.document().put(EMAIL, value);
    }

    public String getPassword() {
        return super.getString(PASSWORD);
    }

    public void setPassword(final String value) {
        this.document().put(PASSWORD, value);
    }

    public String getName() {
        return super.getString(NAME);
    }

    public String getName(final String defVal) {
        final String result = super.getString(NAME);
        return StringUtils.hasText(result) ? result : defVal;
    }

    public void setName(final String value) {
        this.document().put(NAME, value);
    }

    public String getAlias() {
        return super.getString(ALIAS);
    }

    public void setAlias(final String value) {
        this.document().put(ALIAS, value);
    }

    public String getMotto() {
        return super.getString(MOTTO);
    }

    public void setMotto(final String value) {
        this.document().put(MOTTO, value);
    }

    public String getGender() {
        return super.getString(GENDER, "m");
    }

    public void setGender(final String value) {
        this.document().put(GENDER, value);
    }

    public int getAge() {
        return super.getInteger(AGE);
    }

    public void setAge(final int value) {
        this.document().put(GENDER, value);
    }

    public String getAvatar() {
        return super.getString(AVATAR);
    }

    public void setAvatar(final String value) {
        this.document().put(AVATAR, value);
    }

    public String getMobile() {
        return super.getString(MOBILE);
    }

    public void setMobile(final String value) {
        this.document().put(MOBILE, PhoneUtils.sanitize(value));
    }

    public String getPin() {
        return super.getString(PIN);
    }

    public void setPin(final String value) {
        this.document().put(PIN, value);
    }

    public String getAddress() {
        return super.getString(ADDRESS);
    }

    public void setAddress(final String value) {
        this.document().put(ADDRESS, value);
    }

    public String getAddressCity() {
        return super.getString(ADDRESS_CITY);
    }

    public void setAddressCity(final String value) {
        this.document().put(ADDRESS_CITY, value);
    }

    public String getAddressState() {
        return super.getString(ADDRESS_STATE);
    }

    public void setAddressState(final String value) {
        this.document().put(ADDRESS_STATE, value);
    }

    public String getAddressZip() {
        return super.getString(ADDRESS_ZIP);
    }

    public void setAddressZip(final String value) {
        this.document().put(ADDRESS_ZIP, value);
    }

    public String getAddressComplete(final String defVal) {
        final String result = this.getAddressComplete();
        return StringUtils.hasText(result) ? result : defVal;
    }

    public String getAddressComplete() {
        final String name = this.lookUpName();
        final String street = this.getAddress();
        final String zip = this.getAddressZip();
        final String city = this.getAddressCity();
        final String state = this.getAddressState();
        final StringBuilder sb = new StringBuilder();
        if (StringUtils.hasText(street) && StringUtils.hasText(city)) {
            sb.append(name).append("\n");
            sb.append(street).append("\n");
            sb.append(zip).append(" ")
                    .append(city).append(" ")
                    .append("(").append(state).append(")");
        }

        return sb.toString();
    }

    public Document getAddressDocument() {
        final Document document = new Document();
        document.append("name", this.lookUpName());
        document.append("street", this.getAddress());
        document.append("zip", this.getAddressZip());
        document.append("city", this.getAddressCity());
        document.append("state", this.getAddressState());

        return document;
    }

    public boolean getAccountConfirmed() {
        return super.getBoolean(ACCOUNT_CONFIRMED);
    }

    public void setAccountConfirmed(final boolean value) {
        this.document().put(ACCOUNT_CONFIRMED, value);
    }

    public String lookUpName() {
        if (StringUtils.hasText(this.getName())) {
            return this.getName();
        } else if (StringUtils.hasText(this.getAlias())) {
            return this.getAlias();
        } else {
            final String email_name = StringUtils.split(this.getEmail(), "@")[0];
            return StringUtils.split(email_name, ".")[0];
        }
    }

    public List<Integer> getBadges() {
        final List<Integer> badges = super.getArray(BADGES, true);
        if (badges.size() == 0) {
            badges.add(1); // default badge
        }
        return badges;
    }

    public void setBadges(final int value) {
        super.put(BADGES, value);
    }

    public boolean hasBadge(final int badge) {
        return this.getBadges().contains(badge);
    }

    public boolean addBadge(final int badge) {
        if (!this.hasBadge(badge)) {
            this.getBadges().add(badge);
            return true;
        }
        return false;
    }

    public boolean hasValidAddress() {
        return StringUtils.hasText(this.getAddress())
                && StringUtils.hasText(this.getAddressCity())
                && StringUtils.hasText(this.getAddressState())
                && StringUtils.hasText(this.getAddressZip());
    }


    // ------------------------------------------------------------------------
    //                      p r i v a t e
    // ------------------------------------------------------------------------

    private void init() {
        if (!StringUtils.hasText(this.getAlias())) {
            this.setAlias(this.lookUpName());
        }
    }

    // ------------------------------------------------------------------------
    //                      S T A T I C
    // ------------------------------------------------------------------------

    public static String sanitizeUid(final String uid) {
        final String clean_uid = StringUtils.hasText(uid)
                ? StringUtils.replace(uid, " ", "").toLowerCase()
                : "";
        return UID_ALIAS.containsKey(clean_uid) ? UID_ALIAS.get(clean_uid) : clean_uid;
    }

}
