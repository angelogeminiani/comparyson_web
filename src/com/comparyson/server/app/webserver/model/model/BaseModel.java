package com.comparyson.server.app.webserver.model.model;

import com.comparyson.server.app.webserver.model.schema.BaseSchema;
import org.bson.Document;
import org.lyj.commons.logging.Level;
import org.lyj.ext.mongo.model.AbstractModel;
import org.lyj.ext.mongo.schema.AbstractSchema;

import java.util.List;

/**
 * Base class for models
 */
public abstract class BaseModel
        extends AbstractModel {


    public static final String KEYWORDS = BaseSchema.KEYWORDS;

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    public BaseModel(final Class<? extends AbstractSchema> schemaClass) {
        super(schemaClass);
    }

    public BaseModel(final Class<? extends AbstractSchema> schemaClass, final Document document) {
        super(schemaClass, document);
    }

    // ------------------------------------------------------------------------
    //                      p r o t e c t e d
    // ------------------------------------------------------------------------

    /**
     * Return keywords only if field "keywords" is declared in schema.
     *
     * @return List of keywords or null
     */
    public List<String> getKeywords() {
        if (super.hasField(KEYWORDS)) {
            return super.getArrayOfString(KEYWORDS, true);
        } else {
            super.log(Level.WARNING, "getKeywords", "'KEYWORDS' field not declared into schema.");
            return null;
        }
    }

    public List<String> addKeyword(final String keyword) {
        final List<String> keywords = this.getKeywords();
        if (null != keywords) {
            keywords.add(keyword);
        }
        return keywords;
    }


}
