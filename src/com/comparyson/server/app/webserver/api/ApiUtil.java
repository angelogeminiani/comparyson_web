package com.comparyson.server.app.webserver.api;


import com.comparyson.server.app.IConstants;
import org.lyj.commons.util.PathUtils;
import org.lyj.commons.util.StringUtils;
import org.lyj.ext.netty.server.web.HttpServerContext;

public class ApiUtil {

    public static void ping(final HttpServerContext context) {
        context.writeJson("true");
    }

    public static void version(final HttpServerContext context) {
        context.writeJson("{\"app\":\"" + IConstants.APP_VERSION + "\"}");
    }

    public static void redirect(final HttpServerContext context) {
        final String raw_url = context.getParam("url");
        if (StringUtils.hasText(raw_url)) {
            final String url = PathUtils.hasProtocol(raw_url) ? raw_url : "http://" + raw_url;
            context.writeRedirect(url);
        }
    }
}
