package com.comparyson.server.app.webserver.api;

import org.bson.Document;
import org.lyj.commons.util.StringUtils;
import org.lyj.ext.netty.server.web.HttpServerContext;

/**
 * Upload API
 */
public class ApiUpload
        extends AbstractApi {

    // ------------------------------------------------------------------------
    //                      C O N S T
    // ------------------------------------------------------------------------

    private final static String PARAM_USER_ID = "user_id";
    private final static String PARAM_USER_PATH = "user_path";
    private final static String PARAM_BASE64 = "base64";
    private final static String PARAM_FILENAME = "filename";


    private final static String OUT_PARAM_FILENAME = "filename";

    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    public ApiUpload() {

    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------

    public void userAvatarBase64(final HttpServerContext context) {
        final String app_token = super.getParamToken(context);
        super.auth(app_token, (err, valid) -> {
            if (null == err) {
                final String user_id = super.getParam(context, PARAM_USER_ID);
                final String base64 = super.getParam(context, PARAM_BASE64);
                final String user_path = super.getParam(context, PARAM_USER_PATH);
                final String user_filename = super.getParam(context, PARAM_FILENAME);
                if (StringUtils.hasText(user_id) && StringUtils.hasText(base64)) {
                    try {
                        final String response = "";//UploadController.instance().uploadBase64Avatar(user_id, user_path, user_filename, base64, true);
                        super.writeJSON(context, new Document(OUT_PARAM_FILENAME, response));
                    } catch (Throwable t) {
                        super.writeError(context, t, "userAvatarBase64");
                    }
                } else {
                    super.writeErroMissingParams(context, PARAM_USER_ID, PARAM_BASE64);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }


}
