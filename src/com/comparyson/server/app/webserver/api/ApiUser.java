package com.comparyson.server.app.webserver.api;

import com.comparyson.server.app.webserver.model.service.ServiceUser;
import org.bson.Document;
import org.lyj.commons.util.ConversionUtils;
import org.lyj.commons.util.StringUtils;
import org.lyj.ext.netty.server.web.HttpServerContext;

import java.util.List;

/**
 * User API
 */
public class ApiUser extends AbstractApi {

    // ------------------------------------------------------------------------
    //                      C O N S T
    // ------------------------------------------------------------------------

    private final static String PARAM_ID = "id";
    private final static String PARAM_EMAIL = "email";
    private final static String PARAM_PSW = "psw";
    private final static String PARAM_PASSWORD = "password";
    private final static String PARAM_LANG = "lang";

    private final static String PARAM_FACEBOOK_ID = "facebook_id";
    private final static String PARAM_GOOGLE_PLAYER_ID = "google_player_id";
    private final static String PARAM_APPLE_PLAYER_ID = "apple_player_id";
    private final static String PARAM_PARENT_UID = "parent_uid";
    private final static String PARAM_ONLY_CONFIRMED = "only_confirmed";
    private final static String PARAM_MAX_LEVEL = "max_level";

    private final static String PARAM_USER_ID = "user_id";
    private final static String PARAM_PROFILE_ID = "profile_id";
    private final static String PARAM_CONTACT_ID = "contact_id";
    private final static String PARAM_CONTEST_ID = "contest_id";

    private final static String PARAM_FIELD_ID = "field_id";
    private final static String PARAM_FIELD_VALUE = "field_value";

    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    public ApiUser() {

    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------

    public void findById(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String id = super.getParam(context, PARAM_ID);
                if (StringUtils.hasText(id)) {

                    try {
                        final Document response = ServiceUser.instance().findById(id);
                        super.writeJSON(context, response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "findById");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_ID);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void findByEmail(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String email = super.getParam(context, PARAM_EMAIL);
                if (StringUtils.hasText(email)) {

                    try {
                        final Document response = ServiceUser.instance().findByEmail(email);
                        super.writeJSON(context, response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "findByEmail");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_EMAIL);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void exists(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String id = super.getParam(context, PARAM_ID);
                if (StringUtils.hasText(id)) {

                    try {
                        final boolean response = ServiceUser.instance().exists(id);
                        super.writeJSON(context, response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "exists");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_ID);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void remove(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String id = super.getParam(context, PARAM_ID);
                if (StringUtils.hasText(id)) {

                    try {
                        final Document response = ServiceUser.instance().remove(id);
                        super.writeJSON(context, response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "remove");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_ID);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void updateField(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String id = super.getParam(context, PARAM_ID);
                final String field_id = super.getParam(context, PARAM_FIELD_ID);
                final String field_value = super.getParam(context, PARAM_FIELD_VALUE);
                if (StringUtils.hasText(id)) {

                    try {
                        final Document response = ServiceUser.instance().updateField(id, field_id, field_value);
                        super.writeJSON(context, response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "updateField");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_ID, PARAM_FIELD_ID, PARAM_FIELD_VALUE);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void getByParentUid(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String parent_uid = super.getParam(context, PARAM_PARENT_UID);
                final boolean only_confirmed = ConversionUtils.toBoolean(super.getParam(context, PARAM_ONLY_CONFIRMED));
                final int max_level = ConversionUtils.toInteger(super.getParam(context, PARAM_MAX_LEVEL));

                if (StringUtils.hasText(parent_uid)) {

                    try {
                        final List<Document> response = ServiceUser.instance().getByParentUid(parent_uid, only_confirmed, max_level);
                        super.writeJSON(context, response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "getByParentUid");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_PARENT_UID);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void countByParentUid(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String parent_uid = super.getParam(context, PARAM_PARENT_UID);
                final boolean only_confirmed = ConversionUtils.toBoolean(super.getParam(context, PARAM_ONLY_CONFIRMED));
                final int max_level = ConversionUtils.toInteger(super.getParam(context, PARAM_MAX_LEVEL));

                if (StringUtils.hasText(parent_uid)) {

                    try {
                        final long response = ServiceUser.instance().countByParentUid(parent_uid, only_confirmed, max_level);
                        super.writeJSON(context, response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "countByParentUid");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_PARENT_UID);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void count(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {

                try {
                    final long response = ServiceUser.instance().count();
                    super.writeJSON(context, response);
                } catch (Throwable t) {
                    super.writeError(context, t, "count");
                }

            } else {
                super.writeError(context, err);
            }
        });
    }

    public void signin(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String email = super.getParam(context, PARAM_EMAIL);
                final String password = super.getParam(context, PARAM_PSW);
                final String facebook_id = super.getParam(context, PARAM_FACEBOOK_ID);
                final String google_player_id = super.getParam(context, PARAM_GOOGLE_PLAYER_ID);
                final String apple_player_id = super.getParam(context, PARAM_APPLE_PLAYER_ID);

                if ((StringUtils.hasText(email) && StringUtils.hasText(password))
                        || StringUtils.hasText(facebook_id)
                        || StringUtils.hasText(apple_player_id)
                        || StringUtils.hasText(google_player_id)) {

                    try {
                        Document response = null;
                        if (StringUtils.hasText(email) && StringUtils.hasText(password)) {
                            response = ServiceUser.instance().signin(email, password);
                        } else if (StringUtils.hasText(facebook_id)) {
                            response = ServiceUser.instance().signinFacebook(facebook_id);
                        } else if (StringUtils.hasText(apple_player_id)) {
                            response = ServiceUser.instance().signinApplePlayer(apple_player_id);
                        } else if (StringUtils.hasText(google_player_id)) {
                            response = ServiceUser.instance().signinGooglePlayer(google_player_id);
                        }
                        super.writeJSON(context, response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "signin");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_EMAIL, PARAM_PSW, PARAM_FACEBOOK_ID, PARAM_APPLE_PLAYER_ID, PARAM_GOOGLE_PLAYER_ID);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void signup(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String email = super.getParam(context, PARAM_EMAIL);
                final String password = super.getParam(context, PARAM_PASSWORD);
                final String lang = super.getParam(context, PARAM_LANG);
                final String facebook_id = super.getParam(context, PARAM_FACEBOOK_ID);
                final String google_player_id = super.getParam(context, PARAM_GOOGLE_PLAYER_ID);
                final String apple_player_id = super.getParam(context, PARAM_APPLE_PLAYER_ID);
                final String parent_uid = super.getParam(context, PARAM_PARENT_UID);

                if (StringUtils.hasText(email) && StringUtils.hasText(password)) {

                    try {
                        final Document response = ServiceUser.instance().signup(email, password, lang,
                                facebook_id, google_player_id, apple_player_id, parent_uid);
                        super.writeJSON(context, response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "signup");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_EMAIL, PARAM_PASSWORD, PARAM_LANG);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void askResetPassword(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String email = super.getParam(context, PARAM_EMAIL);
                final String password = super.getParam(context, PARAM_PASSWORD); // optional
                if (StringUtils.hasText(email)) {

                    try {
                        final Document response = ServiceUser.instance().askResetPassword(email, password);
                        super.writeJSON(context, response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "askResetPassword");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_EMAIL);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void askConfirmAccount(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String user_id = super.getParam(context, PARAM_USER_ID);
                if (StringUtils.hasText(user_id)) {

                    try {
                        final Document response = ServiceUser.instance().askConfirmAccount(user_id);
                        super.writeJSON(context, response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "askConfirmAccount");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_USER_ID);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void resetPassword(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String id = super.getParam(context, PARAM_ID);
                final String password = super.getParam(context, PARAM_PASSWORD); // optional
                if (StringUtils.hasText(id)) {

                    try {
                        final String html_response = ServiceUser.instance().resetPasswordHTML(id, password);
                        super.writeHTML(context, html_response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "resetPassword");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_ID);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }

    public void confirmAccount(final HttpServerContext context) {
        final String token = super.getParamToken(context);
        super.auth(token, (err, valid) -> {
            if (null == err) {
                final String id = super.getParam(context, PARAM_ID);
                final String email = super.getParam(context, PARAM_EMAIL);
                if (StringUtils.hasText(id) && StringUtils.hasText(email)) {

                    try {
                        final String html_response = ServiceUser.instance().confirmAccountHTML(id, email);
                        super.writeHTML(context, html_response);
                    } catch (Throwable t) {
                        super.writeError(context, t, "confirmAccount");
                    }

                } else {
                    super.writeErroMissingParams(context, PARAM_ID, PARAM_EMAIL);
                }
            } else {
                super.writeError(context, err);
            }
        });
    }



}
