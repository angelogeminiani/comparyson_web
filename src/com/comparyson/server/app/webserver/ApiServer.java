package com.comparyson.server.app.webserver;

import com.comparyson.server.deploy.config.ConfigHelper;
import org.lyj.ext.netty.server.web.HttpServer;

/**
 *
 */
public class ApiServer
        extends HttpServer {

    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    public ApiServer() {
        final int port = ConfigHelper.instance().apiPort();
        super.config().port(port).portAutodetect(false).corsAllowOrigin("*");

        // #1 - add router as first handler
        super.handler(ApiRouter.create(super.config()));

        // #2 - add basic http resource server (serve text and images)
        //super.handler(ResourceHandler.create(this.config()));

    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------

    @Override
    public ApiServer start() {
        super.start();
        super.logger().info("Api Server Started.");
        return this;
    }

    @Override
    public ApiServer stop() {
        super.stop();
        super.logger().info("Api Server stopped.");
        return this;
    }

    // ------------------------------------------------------------------------
    //                      p r i v a t e
    // ------------------------------------------------------------------------


}
