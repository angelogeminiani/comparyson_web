package com.comparyson.server.app.webserver.utils;

import org.lyj.Lyj;
import org.lyj.commons.network.AvatarUtils;
import org.lyj.commons.util.DateUtils;
import org.lyj.commons.util.RandomUtils;

import java.util.List;

/**
 * generates default values
 */
public class DefaultUtils {

    private static final String[] MOTTO = new String[]{"I'm Daisy", "I'm Piggy", "Honky Honky", "I'm Muskio", "Tekken", "Caccolex", "Stay Foolish", "I'll be back!"};

    public static String UUID() {
        return RandomUtils.randomUUID(true);
    }

    public static long getDateLong() {
        return DateUtils.getDateLong();
    }

    public static List<Integer> getDateList() {
        return DateUtils.getDateList();
    }

    public static String getDateString() {
        return DateUtils.getDateString();
    }

    public static int getDateWeek() {
        return DateUtils.getDateWeek();
    }

    public static int getDateDayOfYear() {
        return DateUtils.getDayOfYear();
    }

    public static int getDateDayOfWeek() {
        return DateUtils.getDayOfWeek();
    }

    public static int getDateYear() {
        return DateUtils.getYear();
    }

    public static String getLangCode() {
        return Lyj.getLangCode();
    }

    public static Long getExpirationDateOneWeek() {
        return getExpirationDate(7);
    }

    public static Long getExpirationDateOneDay() {
        return getExpirationDate(1);
    }

    public static Long getExpirationDate(final int durationDays) {
        return getExpirationDate(DateUtils.DAY, durationDays);
    }

    public static Long getExpirationDate(final int measureUnit, final int duration) {
        return DateUtils.postpone(DateUtils.now(), measureUnit, duration).getTime();
    }

    public static String rndMotto() {
        int i = (int) RandomUtils.rnd(0, MOTTO.length - 1);
        if (i > MOTTO.length - 1) {
            i = 0;
        }
        return MOTTO[i];
    }

    public static String rndAvatar() {
        return AvatarUtils.getRobohashUrl(RandomUtils.randomUUID(true));
    }

    public static String rndPin() {
        return RandomUtils.randomNumeric(4);
    }

}
