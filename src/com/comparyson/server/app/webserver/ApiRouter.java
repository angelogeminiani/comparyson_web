package com.comparyson.server.app.webserver;


import com.comparyson.server.app.webserver.api.ApiUser;
import com.comparyson.server.app.webserver.api.ApiUtil;
import org.lyj.ext.netty.server.web.HttpServerConfig;
import org.lyj.ext.netty.server.web.handlers.impl.RoutingHandler;

/**
 * Internal Api router
 */
public class ApiRouter
        extends RoutingHandler {

    // ------------------------------------------------------------------------
    //                      c o n s t
    // ------------------------------------------------------------------------

    private static final String PATH_API = "/api";

    private static final String PATH_API_UTIL = PATH_API.concat("/util");

    private static final String PATH_API_USERS = PATH_API.concat("/users");
    private static final String PATH_API_USER_CHAT_CONTACT = PATH_API.concat("/user_chat_contact");
    private static final String PATH_API_USER_CHAT_MESSAGE = PATH_API.concat("/user_chat_message");
    private static final String PATH_API_CHAT_ROOM = PATH_API.concat("/chat_room");
    private static final String PATH_API_CONTACTS = PATH_API.concat("/contacts");
    private static final String PATH_API_NOTIFICATION = PATH_API.concat("/notification");
    private static final String PATH_API_UPLOAD = PATH_API.concat("/upload");
    private static final String PATH_API_CONTEST = PATH_API.concat("/contest");
    private static final String PATH_API_GAME_SESSION = PATH_API.concat("/game_session");
    private static final String PATH_API_APPS = PATH_API.concat("/apps");
    private static final String PATH_API_BRAND = PATH_API.concat("/brand");
    private static final String PATH_API_WALLET = PATH_API.concat("/wallet");
    private static final String PATH_API_PRIZE = PATH_API.concat("/prize");
    private static final String PATH_API_INVITATION = PATH_API.concat("/invitation");
    private static final String PATH_API_SHOP = PATH_API.concat("/shop");
    private static final String PATH_API_CARD = PATH_API.concat("/card");

    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------

    private final ApiUser _api_users;

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    protected ApiRouter(final HttpServerConfig config) {
        super(config);

        _api_users = new ApiUser();

        this.init();
    }

    // ------------------------------------------------------------------------
    //                      p r i v a t e
    // ------------------------------------------------------------------------

    private void init() {

        super.all(PATH_API.concat("/version")).handler(ApiUtil::version);

        //-- util --//
        super.all(PATH_API_UTIL.concat("/version")).handler(ApiUtil::version);
        super.all(PATH_API_UTIL.concat("/ping")).handler(ApiUtil::ping);
        super.get(PATH_API_UTIL.concat("/redirect/:url")).handler(ApiUtil::redirect);

        //-- upload --//
        //super.post(PATH_API_UPLOAD.concat("/user_avatar_base64")).handler(_api_upload::userAvatarBase64);

        //-- users --//
        super.get(PATH_API_USERS.concat("/:apptoken/:id")).handler(_api_users::findById);
        super.post(PATH_API_USERS.concat("/find_by_id")).handler(_api_users::findById);
        super.post(PATH_API_USERS.concat("/find_by_email")).handler(_api_users::findByEmail);
        super.post(PATH_API_USERS.concat("/exists")).handler(_api_users::exists);
        super.post(PATH_API_USERS.concat("/remove")).handler(_api_users::remove);
        super.post(PATH_API_USERS.concat("/update_field")).handler(_api_users::updateField);
        super.post(PATH_API_USERS.concat("/signin")).handler(_api_users::signin);
        super.post(PATH_API_USERS.concat("/login")).handler(_api_users::signin);
        super.post(PATH_API_USERS.concat("/signup")).handler(_api_users::signup);
        super.post(PATH_API_USERS.concat("/ask_reset_password")).handler(_api_users::askResetPassword);
        super.post(PATH_API_USERS.concat("/ask_confirm_account")).handler(_api_users::askConfirmAccount);
        super.post(PATH_API_USERS.concat("/reset_password")).handler(_api_users::resetPassword);
        super.get(PATH_API_USERS.concat("/reset_password/:apptoken/:id/:password")).handler(_api_users::resetPassword);
        super.post(PATH_API_USERS.concat("/confirm_account")).handler(_api_users::confirmAccount);
        super.get(PATH_API_USERS.concat("/confirm_account/:apptoken/:id/:email")).handler(_api_users::confirmAccount);
        super.get(PATH_API_USERS.concat("/count/:apptoken")).handler(_api_users::count);
        super.get(PATH_API_USERS.concat("/count_by_parent/:apptoken/:only_confirmed/:max_level/:parent_uid")).handler(_api_users::countByParentUid);
        super.get(PATH_API_USERS.concat("/get_by_parent/:apptoken/:only_confirmed/:max_level/:parent_uid")).handler(_api_users::getByParentUid);
        super.post(PATH_API_USERS.concat("/count")).handler(_api_users::count);
        super.post(PATH_API_USERS.concat("/count_by_parent")).handler(_api_users::countByParentUid);
        super.post(PATH_API_USERS.concat("/get_by_parent")).handler(_api_users::getByParentUid);


    }

    // ------------------------------------------------------------------------
    //                      S T A T I C
    // ------------------------------------------------------------------------

    public static ApiRouter create(final HttpServerConfig config) {
        return new ApiRouter(config);
    }


}
