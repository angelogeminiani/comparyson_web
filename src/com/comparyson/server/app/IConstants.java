package com.comparyson.server.app;

import org.lyj.commons.util.CollectionUtils;

import java.util.Map;

/**
 * Application constants
 */
public interface IConstants {

    String APP_VERSION = "1.0.0";

    String HTTP_HOST_URL = "http://services.funnygain.com:4000";

    // ------------------------------------------------------------------------
    //                      APP TOKENS
    // ------------------------------------------------------------------------

    String APP_TOKEN_FUNNY = "funny_trivia_21011968";

    // ------------------------------------------------------------------------
    //                      E R R O R S
    // ------------------------------------------------------------------------

    String[] HANDLED_ERRORS = new String[]{

            IConstants.ERR_USER_NOT_EXISTS,
            IConstants.ERR_USER_EXISTS,
            IConstants.ERR_WRONG_LOGIN,

    };

    //-- system --//
    String ERR_500 = "err_500";
    String ERR_MAILER_DOWN = "err_mailer_down";

    //-- login --//
    String ERR_USER_EXISTS = "err_user_exists"; // user already exists
    String ERR_WRONG_LOGIN = "err_wrong_login";  // wrong username or password
    String ERR_USER_NOT_EXISTS = "err_user_not_exists";  // reset password error


    // ------------------------------------------------------------------------
    //                      F I E L D S
    // ------------------------------------------------------------------------

    String DATE_LONG = "date_long";

    // ------------------------------------------------------------------------
    //                      C O N S T
    // ------------------------------------------------------------------------

    Map<String, String> OS_TYPES = CollectionUtils.toMap("1", "ios", "3", "android");

    String ID_SEP = "_"; // default separator for fields


}
