package com.comparyson.server;

import com.comparyson.server.app.ApplicationServer;
import com.comparyson.server.deploy.config.Deployer;
import com.comparyson.server.deploy.htdocs.HtdocsDeployer;
import org.lyj.Lyj;
import org.lyj.commons.logging.util.LoggingUtils;
import org.lyj.launcher.LyjLauncher;

/**
 * Main server class.
 */
public class Launcher extends LyjLauncher {


    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------

    private boolean _test_mode;

    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    public Launcher(final String[] args) {
        // run Lyj app framework
        super(args);
        _test_mode = super.getArgBoolean("t");
    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------

    @Override
    public void ready() {

        // complete deploy
        new HtdocsDeployer(Lyj.isSilent()).deploy();

        if (!_test_mode) {
            try {
                // deploy App Server
                /*
                Vertx vertx = Vertx.vertx();

                vertx.deployVerticle(new AppServer(), stringAsyncResult -> {
                    // deployed
                    LoggingUtils.getLogger(this).info("Application Server deployed.");
                });        */
                LoggingUtils.getLogger(this).info("LAUNCHER: CREATING APP SERVER INSTANCE");
                ApplicationServer srv = new ApplicationServer(_test_mode);
                LoggingUtils.getLogger(this).info("LAUNCHER: STARTING APP SERVER INSTANCE");
                srv.start();
            } catch (Exception e) {
                e.printStackTrace();
                LoggingUtils.getLogger(this).error("Error Initializing App Server", e);
            }
        } else {
            // TEST MODE: ONLY FOR TEST UNIT
            try {
                ApplicationServer srv = new ApplicationServer(_test_mode);
                srv.start();
            } catch (Exception e) {
                e.printStackTrace();
                LoggingUtils.getLogger(this).error("Error Initializing App Server", e);
            }
        }
    }

    @Override
    public void shutdown() {

    }


    // ------------------------------------------------------------------------
    //                      S T A T I C
    // ------------------------------------------------------------------------

    public static void main(final String[] args) {
        final Launcher main = new Launcher(args);

        Lyj.registerDeployer(new Deployer(Lyj.getConfigurationPath(), Lyj.isSilent()));

        main.run();
    }

}
