package com.comparyson;


import com.comparyson.server.Launcher;

/**
 * TEST UNIT INITIALIZER
 */
public class TestInitializer {

    private static boolean _init = false;

    public static void init() {
        if (!_init) {
            _init = true;

            // register Dictionary extras
            //Dictionary.instance().register(Beta.class);

            // launcher
            Launcher.main(new String[]{"-w", "USERHOME/comparyson_web", "-t", "true"});
        }
    }

}
